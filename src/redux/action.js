

//action creator => trả về 1 function để tránh phải viết các đoạn code lặp lại 
// thì chúng ta chỉ cần tạo ra 1 hàm action creator thôi.

export const addTodo=(data) =>{
    return {
        type: 'todoList/addTodo', 
        payload: data
    }
}