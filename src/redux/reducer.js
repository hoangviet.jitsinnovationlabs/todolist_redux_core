const initState= {
    filter:{
        search: '',
        status: 'All',
        priority: [],
    },
    todoList:[
        {id: 1, name: 'Learn English', completed: false, priority: 'Medium'},
        {id: 2, name: 'Clean house', completed: true, priority: 'High'},
        {id: 3, name: 'Push Up', completed: false, priority: 'Low'}
    ]
}
const rootReducer= (state=initState,action)=>{
    /*
        {
            type: 'todoList/addTodo',
            payload: {   {id: 3, name: 'Eat', completed: false, priority: 'Low'}}  
            //là các thông tin của todo hiện tại mà cta muốn thêm vào 
        }
    */
   console.log({state,action});
    switch(action.type){
        case "todoList/addTodo" : 
            return {
                ...state,    
             todoList: [
                 ...state.todoList,action.payload
             ]
            }
        default: return state; 
    }
}

export default rootReducer